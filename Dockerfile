FROM python:3.11

RUN mkdir /cat_charity_foundation

WORKDIR /cat_charity_foundation

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

RUN chmod a+x /cat_charity_foundation/docker/app.sh