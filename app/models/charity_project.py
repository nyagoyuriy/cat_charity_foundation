from sqlalchemy import Column, String, Text, Integer, Boolean, DateTime

from app.models.base import PreBaseCharityParams

LENGTH = 100


class CharityProject(PreBaseCharityParams):
    name = Column(String(LENGTH), unique=True, nullable=False)
    description = Column(Text, nullable=False)

