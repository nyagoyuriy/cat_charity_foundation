from fastapi import APIRouter

from app.api.endpoints.charity_project import projects_router
from app.api.endpoints.user import user_router
from app.api.endpoints.donation import donation_router

main_router = APIRouter()

main_router.include_router(user_router)

main_router.include_router(projects_router,
                            prefix="/projects",
                            tags=["projects"])

main_router.include_router(donation_router,
                            prefix="/donations",
                            tags=["donations"])
