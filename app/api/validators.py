from http import HTTPStatus

from fastapi import HTTPException
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud.charity_project import crud_charity_project
from app.models import CharityProject
from app.schemas.charity_project import CharityProjectUpdate

EXISTS_ERROR = "Проект с таким именем уже существует!"
NOT_FOUND_ERROR = "Проекта с таким id не существует!"
INVESTED_ERROR = "В проект уже проинвестировали. Его невозможно удалить"
INVESTED_AMOUNT_ERROR = "Сумма необходимых инвестиций не может быть ниже, чем собрано"
CLOSED_PROJECT_ERROR = "В закрытые проекты нельзя вносить изменения"

async def check_name_duplicate(
        name: str, session: AsyncSession
) -> None:
    project_id = await crud_charity_project.get_project_id_by_name(name=name, session=session)
    if project_id is not None:
        raise HTTPException(
            status_code=HTTPStatus.BAD_REQUEST,
            detail=EXISTS_ERROR
        )


async def check_project_exists(
        project_id: int, session: AsyncSession
) -> CharityProject:
    project = await crud_charity_project.get_project_by_id(project_id=project_id, session=session)
    if project is None:
        raise HTTPException(
            status_code=HTTPStatus.BAD_REQUEST,
            detail=NOT_FOUND_ERROR
        )
    return project


async def check_project_was_invested(
        project: CharityProject):
    if project.invested_amount > 0:
        raise HTTPException(
            status_code=HTTPStatus.BAD_REQUEST,
            detail=INVESTED_ERROR
        )

async def check_fully_invested_amount(
        project: CharityProject, full_amount_in: int):
    if project.invested_amount > full_amount_in:
        raise HTTPException(
            status_code=HTTPStatus.BAD_REQUEST,
            detail=INVESTED_AMOUNT_ERROR
        )
async def check_project_was_closed(
        project: CharityProject):
    if project.fully_invested:
        raise HTTPException(
            status_code=HTTPStatus.BAD_REQUEST,
            detail=CLOSED_PROJECT_ERROR
        )


