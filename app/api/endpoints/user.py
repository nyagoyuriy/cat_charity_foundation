from http import HTTPStatus

from fastapi import APIRouter, HTTPException
from fastapi_users import fastapi_users, FastAPIUsers

from app.core.auth import auth_backend
from app.schemas.user import UserRead, UserCreate, UserUpdate
from app.core.manager import fastapi_users


user_router = APIRouter()

user_router.include_router(
    fastapi_users.get_auth_router(auth_backend),
    prefix="/auth/jwt",
    tags=["auth"],
)
user_router.include_router(
    fastapi_users.get_register_router(UserRead, UserCreate),
    prefix="/auth",
    tags=["auth"],
)
user_router.include_router(
    fastapi_users.get_users_router(UserRead, UserUpdate),
    prefix="/users",
    tags=["users"],
)

@user_router.delete("/users/{id}",
    tags=["users"],
    deprecated=True)
async def delete_user(id: str):
    raise HTTPException(
        status_code=HTTPStatus.METHOD_NOT_ALLOWED,
        detail="Удаление пользователей запрещено!"
    )




