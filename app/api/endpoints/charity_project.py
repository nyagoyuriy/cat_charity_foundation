from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.api.validators import check_name_duplicate, check_project_exists, check_project_was_invested, \
    check_fully_invested_amount, check_project_was_closed
from app.core.db import get_async_session
from app.core.manager import current_superuser
from app.crud.charity_project import crud_charity_project
from app.schemas.charity_project import CharityProjectCreate, CharityProjectForUsers, CharityProjectUpdate
from app.services.investing import execute_investment_process

projects_router = APIRouter()

@projects_router.post(
    '/',
    dependencies=[Depends(current_superuser)],)
async def create_charity_project(charity_project: CharityProjectCreate,
                                 session: AsyncSession = Depends(get_async_session)):
    await check_name_duplicate(name=charity_project.name, session=session)
    new_project = await crud_charity_project.create(obj_in=charity_project, session=session)
    await execute_investment_process(object_in=new_project, session=session)
    await session.refresh(new_project)
    return new_project

@projects_router.delete(
    '/{project_id}',
    dependencies=[Depends(current_superuser)],)
async def delete_charity_project(project_id: int,
                                 session: AsyncSession = Depends(get_async_session)):
    project = await check_project_exists(project_id=project_id, session=session)
    await check_project_was_invested(project=project)
    return await crud_charity_project.delete(db_obj=project, session=session)

@projects_router.get(
    '/',
    response_model=list[CharityProjectForUsers],
    response_model_exclude_none=True)
async def get_charity_projects(
        session: AsyncSession = Depends(get_async_session)
):
    return await crud_charity_project.get_all(session=session)

@projects_router.patch(
    '/{project_id}',
    response_model=CharityProjectUpdate,
    dependencies=[Depends(current_superuser)])
async def update_charity_project(
        project_id: int,
        obj_in: CharityProjectUpdate,
        session: AsyncSession = Depends(get_async_session)):
    project = await check_project_exists(project_id=project_id, session=session)
    await check_project_was_closed(project)
    if obj_in.full_amount is not None:
        await check_fully_invested_amount(project, obj_in.full_amount)

    if obj_in.name is not None:
        await check_name_duplicate(name=obj_in.name, session=session)
    return await crud_charity_project.update(db_object=project, obj_in=obj_in, session=session)


