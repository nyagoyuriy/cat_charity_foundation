from fastapi import Depends, APIRouter
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.db import get_async_session
from app.core.manager import current_user, current_superuser
from app.crud.donation import crud_donation
from app.models import User, Donation
from app.schemas.donation import DonationCreate, DonationGetForUser
from app.services.investing import execute_investment_process

donation_router = APIRouter()

@donation_router.post(
    "/")
async def create_donation(
        donate: DonationCreate, session: AsyncSession = Depends(get_async_session), user: User = Depends(current_user)):
    new_donate = await crud_donation.create(donate, session, user)
    await execute_investment_process(object_in=new_donate, session=session)
    await session.refresh(new_donate)
    return new_donate

@donation_router.get(
    "/my",
    response_model=list[DonationGetForUser],
    response_model_exclude_none=True)
async def list_donations_current_user(
    session: AsyncSession = Depends(get_async_session), user: User = Depends(current_user)):
    return await crud_donation.get_all_user_donations(session=session, user=user)

@donation_router.get(
    "/",
    response_model=list[DonationGetForUser],
    response_model_exclude_none=True,
    dependencies=[Depends(current_superuser)])
async def list_donations_all(
    session: AsyncSession = Depends(get_async_session)):
    return await crud_donation.get_all(session=session)
