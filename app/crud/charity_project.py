from typing import Optional

from fastapi.encoders import jsonable_encoder
from sqlalchemy import select, inspect
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud.base import CRUDBase
from app.models import CharityProject
from app.schemas.charity_project import CharityProjectUpdate


class CRUDCharityProject(CRUDBase):
    async def get_project_id_by_name(
            self, name: str, session: AsyncSession
    ) -> Optional[int]:
        projects = await session.execute(
            select(self.model).where(self.model.name == name)
        )
        return projects.scalars().first()

    async def get_project_by_id(
            self, project_id: int, session: AsyncSession):
        return await self.get(project_id, session)

    async def update(
            self, db_object: CharityProject, obj_in: CharityProjectUpdate, session: AsyncSession):
        obj_data = jsonable_encoder(db_object)
        update_data = obj_in.dict(exclude_unset=True)
        for field in obj_data:
            if field in update_data:
                setattr(db_object, field, update_data[field])
        await session.commit()
        await session.refresh(db_object)
        return db_object



crud_charity_project = CRUDCharityProject(CharityProject)