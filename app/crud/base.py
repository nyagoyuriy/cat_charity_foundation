from typing import Optional

from sqlalchemy import select, false
from sqlalchemy.ext.asyncio import AsyncSession

from app.models import User


class CRUDBase:
    def __init__(self, model):
        self.model = model

    async def get(self, obj_id: int, session: AsyncSession):
        db_obj = await session.execute(
            select(self.model).where(self.model.id == obj_id)
        )
        return db_obj.scalars().first()

    async def create(
            self, obj_in, session: AsyncSession, user: Optional[User] = None):
        obj_in_data = obj_in.dict()

        if user is not None:
            obj_in_data["user_id"] = user.id

        db_obj = self.model(**obj_in_data)
        session.add(db_obj)
        await session.commit()
        await session.refresh(db_obj)
        return db_obj

    async def delete(
            self, db_obj, session: AsyncSession):
        await session.delete(db_obj)
        await session.commit()
        return db_obj

    async def get_all(
            self, session: AsyncSession):
        db_objs = await session.execute(
            select(self.model)
        )
        return db_objs.scalars().all()

    async def get_all_unclosed(
            self, session: AsyncSession):
        db_objs = await session.execute(
            select(self.model).where(self.model.fully_invested == false()).order_by(self.model.create_date)
        )
        return db_objs.scalars().all()
