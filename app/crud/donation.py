from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud.base import CRUDBase
from app.models import Donation, User


class CRUDDonation(CRUDBase):
    async def get_all_user_donations(
            self, session: AsyncSession, user: User):
        db_donations = await session.execute(
            select(Donation).where(Donation.user_id == user.id)
        )
        return db_donations.scalars().all()



crud_donation = CRUDDonation(Donation)