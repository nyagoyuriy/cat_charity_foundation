from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field, PositiveInt

MIN_LEN = 1
MAX_LEN = 100

class CharityProjectBase(BaseModel):
    name: Optional[str] = Field(None, min_length=MIN_LEN, max_length=MAX_LEN)
    description: Optional[str] = Field(None, min_length=MIN_LEN)
    full_amount: Optional[PositiveInt] = Field(None)


class CharityProjectCreate(CharityProjectBase):
    name: str = Field(min_length=MIN_LEN, max_length=MAX_LEN)
    description: str = Field(default=..., min_length=MIN_LEN)
    full_amount: PositiveInt


class CharityProjectForUsers(CharityProjectCreate):
    id: int
    invested_amount: int
    fully_invested: bool
    create_date: datetime
    close_date: Optional[datetime]


class CharityProjectUpdate(CharityProjectBase):
    pass


"""
    full_amount = Column(Integer, nullable=False)
    invested_amount = Column(Integer, default=0)
    fully_invested = Column(Boolean, default=False)
    create_date = Column(DateTime, default=datetime.now)
    close_date = Column(DateTime)
"""