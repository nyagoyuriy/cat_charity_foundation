from datetime import datetime
from typing import Optional

from pydantic import BaseModel, PositiveInt, Field


class DonationCreate(BaseModel):
    comment: Optional[str] = Field(default=None)
    full_amount: PositiveInt

class DonationGetForUser(DonationCreate):
    id: int
    invested_amount: int
    fully_invested: bool
    create_date: datetime
    close_date: Optional[datetime]