from fastapi import FastAPI

from app.api.endpoints.charity_project import projects_router
from app.api.endpoints.donation import donation_router
from app.api.routers import main_router
from app.core.init_db import create_superuser
from app.api.endpoints.user import user_router

app: FastAPI = FastAPI()
app.include_router(main_router)


@app.on_event("startup")
async def startup():
    await create_superuser()
