from datetime import datetime
from typing import Union

from sqlalchemy.ext.asyncio import AsyncSession

from app.crud.charity_project import crud_charity_project
from app.crud.donation import crud_donation
from app.models import Donation, CharityProject

async def close_object(obj) -> None:
    obj.fully_invested = True
    obj.close_date = datetime.now()

async def execute_investment_process(
        object_in: Union[CharityProject, Donation], session: AsyncSession):
    crud_model = crud_donation if isinstance(object_in, CharityProject) else crud_charity_project
    unclosed_objs = await crud_model.get_all_unclosed(session=session)
    available_amount = object_in.full_amount
    for inv_obj in unclosed_objs:
        delta = inv_obj.full_amount - inv_obj.invested_amount
        current_invest = min(delta, available_amount)
        inv_obj.invested_amount += current_invest
        object_in.invested_amount += current_invest
        available_amount -= current_invest

        if inv_obj.full_amount == inv_obj.invested_amount:
            await close_object(inv_obj)

        if object_in.full_amount == object_in.invested_amount:
            await close_object(object_in)
            break

    await session.commit()

