from typing import AsyncGenerator

from fastapi import Depends
from fastapi_users_db_sqlalchemy import SQLAlchemyUserDatabase

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from app.core.db_config import URL_DB


engine = create_async_engine(URL_DB)
async_session_maker = sessionmaker(engine, class_=AsyncSession)


async def get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as async_session:
        yield async_session


async def get_user_db(session: AsyncSession = Depends(get_async_session)):
    from app.models import User
    yield SQLAlchemyUserDatabase(session, User)


