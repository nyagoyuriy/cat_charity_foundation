import contextlib

from fastapi_users.exceptions import UserAlreadyExists
from sqlalchemy import text

from werkzeug.security import generate_password_hash

from app.core.db_config import SUPER_USER_EMAIL, SUPER_USER_PASSWORD
from app.core.db import get_async_session, get_user_db
from app.core.manager import get_user_manager
from app.schemas.user import UserCreate

get_async_session_context = contextlib.asynccontextmanager(get_async_session)
get_user_db_context = contextlib.asynccontextmanager(get_user_db)
get_user_manager_context = contextlib.asynccontextmanager(get_user_manager)

async def create_superuser():
    email = SUPER_USER_EMAIL
    password = SUPER_USER_PASSWORD

    try:
        async with get_async_session_context() as session:
            async with get_user_db_context(session) as user_db:
                async with get_user_manager_context(user_db) as user_manager:
                    await user_manager.create(
                        UserCreate(email=email, password=password, is_superuser=True)
                    )
    except UserAlreadyExists:
        pass