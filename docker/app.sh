#!/bin/bash

alembic upgrade head

cd /cat_charity_foundation

gunicorn app.main:app --workers 4 --worker-class uvicorn.workers.UvicornWorker --bind=0.0.0.0:8000